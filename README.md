# HealthForge InfoSec Quiz

Hello! Thanks for taking the time to do our infosec technical test. We know these are annoying, but we need a way to assess everyone that we employ to avoid disappointment on both sides later on.

Please follow the instructions below and when you are done, put your answer into a *private* git repo and send us a link to it for us to assess - add the user `mmsinclair` on either GitHub or BitBucket.

Feel free to use as much Google-Fu as you need to (that is what we do on a daily basis) - however, you need to do the test on your own. Any outside help is not in the spirit of the test and will result in seven or eight years of bad luck, depending on which is worst for you.

**Impress us with your skills and show us what you can do! This is your chance to shine and the more we see the better.**

# Pre-requisites

You will need:

- an IDE or text editor of your choice
- web browser
- git

Please answer the following questions and put your answers in an appropriately formatted text file in your git repo.

# Question 1

## 1.1 SQL Injection

* Provide a snippet of code in a language that you are comfortable with that is vulnerable to SQL injection.
* Then, explain in a paragraph how it could be protected from SQL injection.

## 1.2 Docker Security

* Explain in a paragraph how docker containers could be tested for vulnerabilities.
* Provide another paragraph explaining how this could be automated.

## 1.3 Social Engineering

* Describe the best method in your opinion to get an employee of HealthForge to divulge their Amazon Web Services login credentials.
* Suggest 5 simple and practical steps HealthForge could employ to prevent the attack you described above.

## Question 2

Scenario: a customer of HealthForge is creating an app that is used for assessments for newly expectant mothers. It needs to capture the mother's personal details, as well as those of the father and her relationship to him. Blood screening tests for HIV and other STIs must be recorded, as well as dates of the mother's menstrual cycle. When a patient attends, a record of the appointment will be captured in the system as a structured form.

The users of the app must be able to:

* Search for patients by the mother's name.
* Display reports that show age distributions of mothers with HIV, filtered by postcode.
* Continuously calculate the distribution of the time from the last known menstrual cycle and the date of the first appointment using code written by a PhD student at Kings College Hospital.

The app will be trialled across all departments, including private services, at St Mary's Hospital, Imperial College Healthcare NHS Trust, London. Their patients are from all walks of life in North-West London: rich, poor, celebrities and the common man.

*Question*

HealthForge's customer has asked us to provide consulting advice on data privacy for this app. We have asked you to provide an initial briefing for internal review at HealthForge, the will form the basis of our response to the customer.

Provide a short response (3 - 5 paragraphs) to the customer advising them on best practice for data privacy in their app, given its sensitive nature and how they might manage their initial trial deployment at St Mary's.

## Question 3

Summarise the data privacy laws for handling and storing personally identifiable digital data in Portugal and India.

Provide a short comparison between the regulations for both countries that would be relevant to HealthForge customers.

## Question 4

## 4.1 ISO27001

* List the top 5 circumventions of ISO27001 controls based on your experience of real-world implementations.
* List the top 5 frustrations employees experience when implementing ISO27001.


## 4.2 Software Development

* Describe in a paragraph how HealthForge could integrate clinical safety reviews into an agile software development cycle.
* Provide a simple 10 point checklist that can be added to the Definition of Ready for any developer of healthcare software that takes into account general data privacy and information security best practice.


## 4.3 Internal Breaches

* Describe a common scenario where personally identifiable and sensitive healthcare data hosted by HealthForge on behalf of our customers might be stolen by internal employees. Provide reasons for why employees might take this data.
* Describe 3 simple practical steps that could be employed by HealthForge to prevent this type of internal data breach.